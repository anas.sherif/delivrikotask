package com.anas.delivrikotask.screens.ask_doctor

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import com.anas.delivrikotask.base.activity.BaseActivity

class AskDoctorActivity : BaseActivity() {

    private val mViewModel by lazy { ViewModelProviders.of(this)[AskDoctorViewModel::class.java] }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

}
        
        
