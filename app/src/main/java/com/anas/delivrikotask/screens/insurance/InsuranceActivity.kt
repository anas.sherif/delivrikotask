package com.anas.delivrikotask.screens.insurance

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import com.anas.delivrikotask.R
import com.anas.delivrikotask.base.activity.BaseActivity
import com.anas.delivrikotask.data.models.InsuranceCompany
import com.anas.delivrikotask.screens.ask_doctor.AskDoctorActivity
import com.anas.delivrikotask.screens.insurance.adapter.InsuranceCompaniesAdapter
import com.anas.delivrikotask.viewmodel.isnurance.InsuranceViewModel
import kotlinx.android.synthetic.main.activity_insurance.*
import kotlinx.android.synthetic.main.layout_toolbar.*

class InsuranceActivity : BaseActivity(), InsuranceCompaniesAdapter.OnCompanyClickListener {

    private val mViewModel by lazy { ViewModelProviders.of(this)[InsuranceViewModel::class.java] }

    private val insuranceCompaniesAdapter by lazy { InsuranceCompaniesAdapter(mutableListOf(), this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_insurance)
        initToolbar()
        initAdapter()
        initInsuranceCompaniesLiveData()
    }

    private fun initToolbar() {
        tvTitle.text = getString(R.string.title_insurance)
    }

    /*Init the companies adapter*/
    private fun initAdapter() {
        with(rvCompanies) {
            layoutManager = GridLayoutManager(this@InsuranceActivity, 2)
            adapter = insuranceCompaniesAdapter
        }
    }

    /* Update the adapter when the data is ready */
    private fun updateAdapter(insuranceCompanies: MutableList<InsuranceCompany>?) {
        insuranceCompanies?.let {
            insuranceCompaniesAdapter.updateAdapter(it)
        }
    }

    /* Init the insurance company observer */
    private fun initInsuranceCompaniesLiveData() {
        val observer = Observer<MutableList<InsuranceCompany>> {
            updateAdapter(it)
        }

        mViewModel.insuranceCompaniesLiveData.observe(this, observer)
        mViewModel.getInsuranceCompanies()
    }

    override fun onCompanyClick(insuranceCompany: InsuranceCompany) {
        startActivity(Intent(this, AskDoctorActivity::class.java))
    }
}