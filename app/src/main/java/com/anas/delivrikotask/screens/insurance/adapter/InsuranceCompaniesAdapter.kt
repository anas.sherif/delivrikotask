package com.anas.delivrikotask.screens.insurance.adapter

import com.anas.delivrikotask.R
import com.anas.delivrikotask.base.adapter.BaseRVAdapter
import com.anas.delivrikotask.base.adapter.BaseViewHolder
import com.anas.delivrikotask.data.models.InsuranceCompany
import kotlinx.android.synthetic.main.item_insurance_company.view.*

class InsuranceCompaniesAdapter(
    private val insuranceCompanyList: MutableList<InsuranceCompany>,
    private val onCompanyClickListener: OnCompanyClickListener
) :
    BaseRVAdapter(R.layout.item_insurance_company) {
    override fun getItemCount(): Int {
        return insuranceCompanyList.size
    }

    override fun onBindViewHolder(viewHolder: BaseViewHolder, position: Int) {
        with(viewHolder.itemView) {
            val insuranceCompany = insuranceCompanyList[position]
            ivIcon.setImageResource(insuranceCompany.icon)

            setOnClickListener { onCompanyClickListener.onCompanyClick(insuranceCompany) }
        }
    }

    fun updateAdapter(insuranceCompanies: MutableList<InsuranceCompany>) {
        insuranceCompanyList.clear()
        insuranceCompanyList.addAll(insuranceCompanies)
    }

    /* Interface to interact with the activity */
    interface OnCompanyClickListener {
        fun onCompanyClick(insuranceCompany: InsuranceCompany)
    }
}