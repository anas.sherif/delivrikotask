package com.anas.delivrikotask.screens.ask_doctor


import android.arch.lifecycle.ViewModel

class AskDoctorViewModel : ViewModel() {

    private val mAskDoctorBusiness by lazy { AskDoctorBusiness() }
}