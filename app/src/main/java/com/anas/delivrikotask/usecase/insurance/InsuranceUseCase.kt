package com.anas.delivrikotask.usecase.insurance

import android.arch.lifecycle.MutableLiveData
import com.anas.delivrikotask.R
import com.anas.delivrikotask.data.models.InsuranceCompany
import com.anas.delivrikotask.repo.insurance.InsuranceRepositoryImpl

class InsuranceUseCase {

    private val mInsuranceRepository = InsuranceRepositoryImpl()

    val insuranceCompaniesLiveData by lazy { MutableLiveData<MutableList<InsuranceCompany>>() }

    fun getInsuranceCompanies() {
        val insuranceCompanyList = createCompanyList()
        insuranceCompaniesLiveData.postValue(insuranceCompanyList)
    }

    private fun createCompanyList(): MutableList<InsuranceCompany> {
        val insuranceCompanyList = mutableListOf<InsuranceCompany>()
        for (index in 0..5) {
            val company = InsuranceCompany(R.mipmap.ic_launcher)
            insuranceCompanyList.add(company)
        }
        return insuranceCompanyList
    }
}