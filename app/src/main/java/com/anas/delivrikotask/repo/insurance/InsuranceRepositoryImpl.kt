package com.anas.delivrikotask.repo.insurance

/*
* Repo is used to handle API requests and a subscription on Observable or Single classes.
* We can use  repo for a network calls and another repo for cached data.
*/
class InsuranceRepositoryImpl : InsuranceRepository {

    private val mInsuranceClient = InsuranceClientImpl()

}