package com.anas.delivrikotask.receivers

import android.annotation.SuppressLint
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import com.anas.delivrikotask.livedata.NetworkChangeLiveData

/*
* Network change receiver to listen on the change occurs on the network
* */
class NetworkChangeReceiver : BroadcastReceiver() {

    private val networkChangeLiveData by lazy { NetworkChangeLiveData.instance }

    @SuppressLint("UnsafeProtectedBroadcastReceiver")
    override fun onReceive(contex: Context?, intent: Intent?) {
        val cm = contex?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val info = cm.activeNetworkInfo
        networkChangeLiveData.update(info != null && info.isConnected)
    }

}