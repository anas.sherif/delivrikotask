package com.anas.delivrikotask.viewmodel.isnurance


import android.arch.lifecycle.ViewModel
import com.anas.delivrikotask.usecase.insurance.InsuranceUseCase

class InsuranceViewModel : ViewModel() {

    private val mInsuranceBusiness by lazy { InsuranceUseCase() }

    val insuranceCompaniesLiveData by lazy { mInsuranceBusiness.insuranceCompaniesLiveData }

    fun getInsuranceCompanies() {
        mInsuranceBusiness.getInsuranceCompanies()
    }
}