package com.anas.delivrikotask.livedata

import android.arch.lifecycle.LiveData

/*
* Network change live date with a boolean of true or false*/
class NetworkChangeLiveData : LiveData<Boolean>() {

    companion object {
        val instance = NetworkChangeLiveData()
    }

    @Synchronized
    fun update(isConnected: Boolean) {
        postValue(isConnected)
    }


}