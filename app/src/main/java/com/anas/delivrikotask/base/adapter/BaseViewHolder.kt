package com.anas.delivrikotask.base.adapter

import android.support.v7.widget.RecyclerView
import android.view.View

open class BaseViewHolder(view: View) : RecyclerView.ViewHolder(view)