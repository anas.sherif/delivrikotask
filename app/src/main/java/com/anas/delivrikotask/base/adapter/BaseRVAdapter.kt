package com.anas.delivrikotask.base.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup

abstract class BaseRVAdapter(var layout: Int) : RecyclerView.Adapter<BaseViewHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, p1: Int): BaseViewHolder {
        val view = LayoutInflater.from(viewGroup.context).inflate(layout, viewGroup, false)
        return BaseViewHolder(view)
    }

    abstract override fun getItemCount(): Int

    abstract override fun onBindViewHolder(viewHolder: BaseViewHolder, position: Int)

}