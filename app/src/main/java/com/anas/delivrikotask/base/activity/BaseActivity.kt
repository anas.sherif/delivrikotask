package com.anas.delivrikotask.base.activity

import android.content.IntentFilter
import android.os.Bundle
import android.os.PersistableBundle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import com.anas.delivrikotask.R
import com.anas.delivrikotask.receivers.NetworkChangeReceiver

abstract class BaseActivity : AppCompatActivity() {

    private val myConnectivityReceiver by lazy { NetworkChangeReceiver() }

    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onCreate(savedInstanceState, persistentState)
        initNetworkChange()
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(myConnectivityReceiver)
    }


    /*
    * This function is used to init network change receiver
    * */
    private fun initNetworkChange() {
        val filter = IntentFilter()
        filter.addAction("android.net.conn.CONNECTIVITY_CHANGE")
        registerReceiver(myConnectivityReceiver, filter)
    }
}